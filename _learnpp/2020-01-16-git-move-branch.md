---
layout: learnpp
title: Git Move Branch
status: publish
published: true
author:
  display_name: dcodernz
  login: dcodernz
  email: no-reply@thh.com
  url: http://www.thheuer.com
author_login: dcodernz
author_email: no-reply@thh.com
author_url: http://www.thheuer.com
categories:
- Computer
- Science
- Ideas
- Factory
- Git
- Versioning
tags: [Computer, Science, Ideas, Factory, Git, Versioning]
comments: []
---
Want to move a branch in Git because another branch with the same name exists?

### git branch -m newbranchname

You may have created the same branch name in your local repository called branchname.
You want to move it. This is how:

```git branch -m newbranchname```

if you are on the branch currently.

Otherwise:

```git branch -m branchname newbranchname```
