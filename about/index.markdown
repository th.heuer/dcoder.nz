---
layout: page
status: publish
permalink: /about/
published: true
title: about
author:
  display_name: dcodernz
  login: admin
  email: dcodernz@ihostnz.com
  url: ''
author_login: admin
author_email: dcodernz@ihostnz.com
wordpress_id: 2
wordpress_url: http://www.ihostnz.com/blog/?page_id=2
date: '2010-07-24 12:20:46 +1200'
date_gmt: '2010-07-24 12:20:46 +1200'
categories: []
tags: []
---
# dcoder.nz

This is a website about software development and related topics such as programming, coding and algorithms and datastructures.
It is provided as is to help you become a better programmer and web developer.