<style>
 
body {
	background-color: #333;
}
/* LIST #2 */
#list2 { width:320px; }
#list2 ol { font-style:italic; font-family:Georgia, Times, serif; font-size:24px; color:#bfe1f1;  }
#list2 ol li { }
#list2 ol li p { padding:8px; font-style:normal; font-family:Arial; font-size:13px; color:#eee; border-left: 1px solid #999; }
#list2 ol li p em { display:block; }
h1,a { color: #eee; }
</style>

<h1>Templates</h1>
<div id="list2">
   <ol>
<?php

$dir = dirname(__FILE__);

foreach (new DirectoryIterator('.') as $fileInfo) {
	if($fileInfo->isDot()) continue;
	$filename = $fileInfo->getFilename();
	if ($filename == 'docs' || strpos($filename, '.') === 0 || strpos($filename, '.php') !== false) continue;
	//echo "$filename<br>\n";
	?><li><p><em>Demo of</em> <a href="<?php echo $filename; ?>/"><?php echo ucfirst($filename); ?></a></p></li><?php
}
?>
   </ol>
</div>
