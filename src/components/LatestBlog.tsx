import React from 'react'
import Typography from "@mui/material/Typography";
import {Link} from "gatsby";

interface Props extends React.PropsWithChildren {
    data: unknown
}
export const LatestBlog = (props: Props) => {
    const link = `/blog/${encodeURIComponent(props.data.allMarkdownRemark.nodes[0].parent.name)}`
    return (
        <Link to={link} className="latest-blog-link">
            <Typography variant="h3">
                Latest Blog: {props.data.allMarkdownRemark.nodes[0].fields.title}
            </Typography>
            <Typography variant="h4">
                {props.data.allMarkdownRemark.nodes[0].fields.fileDate}
            </Typography>
            <Typography variant="body1">
                {props.data.allMarkdownRemark.nodes[0].fields.fileDate}
            </Typography>
        </Link>
    )
}
