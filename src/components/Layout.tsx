import * as React from 'react';
import logo from '../assets/dcoder-logo-orange.svg';
import logoWhite from '../assets/dcoder-logo-white.svg';
import '../styles.scss';
import {PropsWithChildren} from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

import {createTheme, Theme, ThemeProvider} from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import {Switch} from "@mui/material";
import HideOnScroll from "./HideOnScroll";
import LabelBottomNavigation from "./LabelBottomNavigation";
import {Link} from "gatsby";
import {useThemeStore} from "../store/theme";
// Define the custom colors for the newspaper-like theme.
const newspaperColors = {
    black: '#1a1a1a',  // Rich black color for text.
    white: '#ffffff',  // Pure white for backgrounds.
    lightGrey: '#f1f1f1',  // Light grey for paper-like surfaces.
    mediumGrey: '#d7d7d7',  // Medium grey for slight contrast.
    darkGrey: '#595959',  // Dark grey for deep shadows or text.
};

const getTheme = (mode: 'light' | 'dark') =>
{
    const theme = {
        palette: {
            mode: mode,
            primary: {
                main: mode === 'dark' ? '#eee' : '#d27a30',
            },
            background: {
                default: mode === 'dark' ? newspaperColors.black : newspaperColors.lightGrey,
                paper: mode === 'dark' ? newspaperColors.darkGrey : newspaperColors.white,
            },
            text: {
                primary: mode === 'dark' ? newspaperColors.lightGrey : newspaperColors.black,
                secondary: mode === 'dark' ? newspaperColors.mediumGrey : newspaperColors.darkGrey,
                white: newspaperColors.white,
            },
        },
    };
    type B = typeof theme;
    type A = B & Theme;
    return createTheme(theme) as A;
};

interface Props extends PropsWithChildren {
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    window?: () => Window;
}

const drawerWidth = 240;
const navItems = [
    {
        link: '/blog',
        title: 'Blog',
    },
    {
        link: '/learnpp',
        title: 'learn++',
    },
];

export default function DrawerAppBar(props: Readonly<Props>) {
    const { window } = props;
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen((prevState) => !prevState);
    };

    const drawer = (
        <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
            <Typography variant="h6" sx={{ my: 2 }}>
                dcoder.nz
            </Typography>
            <Divider />
            <List>
                {navItems.map((item) => (
                    <ListItem key={item.link} disablePadding>
                        <ListItemButton sx={{ textAlign: 'center' }}>
                            <ListItemText primary={item.title} />
                        </ListItemButton>
                    </ListItem>
                ))}
            </List>
        </Box>
    );

    const container = window !== undefined ? () => window().document.body : undefined;
    const {name, setTheme} = useThemeStore();

    const toggleDarkMode = () => {
        setTheme(name === 'light' ? 'dark' : 'light');
    };

    // Instantiate the theme with the current mode.
    const myTheme = React.useMemo(() => getTheme(name), [name]);

    return (
        <ThemeProvider theme={myTheme}>
            <CssBaseline /> {/* Provides a baseline CSS reset */}
            {/* Content of your application goes here, which will be wrapped in the provided theme. */}
            <Box sx={{ display: 'flex', marginBottom: '32px' }} className={`${name}-theme`}>
                <HideOnScroll>
                    <AppBar component="nav">
                        <Toolbar>
                                <IconButton
                                    color="inherit"
                                    aria-label="open drawer"
                                    edge="start"
                                    onClick={handleDrawerToggle}
                                    sx={{ mr: 2, display: { sm: 'none' } }}
                                >
                                    <MenuIcon />
                                </IconButton>
                                <Typography
                                    variant="h6"
                                    component="div"
                                    sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
                                >
                                    <Link to="/">
                                        <img src={name === 'dark' ? logo : logoWhite} alt="logo" style={{ width: '40px'}} />
                                        <Button sx={{color: myTheme.palette.text.white}}>coder.nz</Button>
                                    </Link>
                                </Typography>
                            <Box sx={{display: {xs: 'none', sm: 'block'}}}>
                                {navItems.map((item) => (
                                    <Link key={item.link} to={item.link}>
                                        <Button sx={{color: myTheme.palette.text.white}}>
                                            {item.title}
                                        </Button>
                                    </Link>
                                ))}
                                <Switch checked={name === "dark"} onChange={toggleDarkMode} />
                            </Box>
                        </Toolbar>
                    </AppBar>
                </HideOnScroll>
                <nav>
                <Drawer
                        container={container}
                        variant="temporary"
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                        sx={{
                            display: { xs: 'block', sm: 'none' },
                            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                        }}
                    >
                        {drawer}
                    <Switch checked={name === "dark"} onChange={toggleDarkMode} />
                    </Drawer>
                </nav>
                <Box component="main" sx={{ p: 0, pt: 1, width: '100%' }}>
                    <Toolbar />
                    <div className="center-children">
                        {props.children}
                    </div>
                </Box>
            </Box>
            <LabelBottomNavigation />
        </ThemeProvider>
    );
}
