export const quotes = [
    "Leadership is a potent combination of strategy and character. But if you must be without one, be without the strategy. – Norman Schwarzkopf",
    "A meeting is an event at which the minutes are kept and the hours are lost. – Unknown",
    "You are not your resume, you are your work. – Seth Godin",
    "Beware of any enterprise requiring new clothes. – Henry Thoreau",
    "One finds limits by pushing them. – Herbert Simon",
    "If you see a bandwagon, it’s too late. – James Goldsmith",
    "Business opportunities are like buses, there’s always another one coming. – Richard Branson",
    "You must be the change you wish to see in the world. – Mahatma Gandhi",
    "You only have to do a very few things right in your life so long as you don’t do too many things wrong. – Warren Buffett",
    "The trick is in what one emphasizes. We either make ourselves miserable, or we make ourselves strong. The amount of work is the same. – Carlos Castaneda",
    "There’s no shortage of remarkable ideas, what’s missing is the will to execute them. – Seth Godin",
    "The great accomplishments of man have resulted from the transmission of ideas of enthusiasm. – Thomas J. Watson",
    "Everything should be made as simple as possible, but not simpler. – Albert Einstein",
    "Far and away the best prize that life offers is the chance to work hard at work worth doing. – Theodore Roosevelt",
    "Change is not a threat, it’s an opportunity. Survival is not the goal, transformative success is. – Seth Godin",
    "Even if you are on the right track, You’ll get run over if you just sit there. – Will Rogers",
    "You must either modify your dreams or magnify your skills. – Jim Rohn",
    "Look well to this day. Yesterday is but a dream and tomorrow is only a vision. But today well lived makes every yesterday a dream of happiness and every tomorrow a vision of hope. Look well therefore to this day. – Francis Gray",
    "Imagination is everything. It is the preview of life’s coming attractions. – Albert Einstein",
    "Leadership is the art of getting someone else to do something you want done because he wants to do it. – Dwight Eisenhower",
    "Never interrupt your enemy when he is making a mistake. – Napoleon Bonaparte",
    "To be successful, you have to have your heart in your business, and your business in your heart. – Sr. Thomas Watson",
    "Every accomplishment starts with a decision to try. – Unknown",
    "The first one gets the oyster the second gets the shell. – Andrew Carnegie",
    "Whether you think you can or whether you think you can’t, you’re right! – Henry Ford",
    "A business has to be involving, it has to be fun, and it has to exercise your creative instincts. – Richard Branson",
    "We generate fears while we sit. We over come them by action. Fear is natures way of warning us to get busy. – Dr. Henry Link",
    "People rarely buy what they need. They buy what they want. – Seth Godin",
    "Live daringly, boldly, fearlessly. Taste the relish to be found in competition – in having put forth the best within you. – Henry J. Kaiser",
    "Success is often achieved by those who don’t know that failure is inevitable. – Coco Chanel",
    "A man should never neglect his family for business. – Walt Disney",
    "Sometimes when you innovate, you make mistakes. It is best to admit them quickly and get on with improving your other innovations. – Steve Jobs",
    "The successful man is the one who finds out what is the matter with his business before his competitors do. – Roy L. Smith",
    "Business is more exciting than any game. – Lord Beaverbrook",
    "The winners in life think constantly in terms of I can, I will, and I am. Losers, on the other hand, concentrate their waking thoughts on what they should have or would have done, or what they can’t do. – Dennis Waitley",
    "Work expands so as to fill the time available for its completion. – Cyril Northcote Parkinson/Parkinson’s Law.",
    "I feel that luck is preparation meeting opportunity. – Oprah Winfrey",
    "Ideas in secret die. They need light and air or they starve to death. – Seth Godin",
    "The true entrepreneur is a doer, not a dreamer. – Unknown",
    "My son is now an ‘entrepreneur’. That’s what you’re called when you don’t have a job. – Ted Turner",
    "The man who will use his skill and constructive imagination to see how much he can give for a dollar, instead of how little he can give for a dollar, is bound to succeed. – Henry Ford",
    "For all of its faults, it gives most hardworking people a chance to improve themselves economically, even as the deck is stacked in favor of the privileged few. Here are the choices most of us face in such a system: Get bitter or get busy. – Bill O’Reilly",
    "Please think about your legacy, because you’re writing it every day. – Gary Vaynerchuck",
    "Victory goes to the player who makes the next–to–last mistake. – Savielly Grigorievitch Tartakower",
    "I like thinking big. If you’re going to be thinking anything, you might as well think big. – Donald Trump",
    "Surviving a failure gives you more self–confidence. Failures are great learning tools… but they must be kept to a minimum. – Jeffrey Immelt",
    "To think is easy. To act is difficult. To act as one thinks is the most difficult. – Johann Wolfgang Von Goethe",
    "Yesterday’s home runs don’t win today’s games. – Babe Ruth",
    "The only way around is through. – Robert Frost",
    "The new source of power is not money in the hands of a few, but information in the hands of many. – John Naisbitt",
    "The critical ingredient is getting off your butt and doing something. It’s as simple as that. A lot of people have ideas, but there are few who decide to do something about them now. Not tomorrow. Not next week. But today. The true entrepreneur is a doer, not a dreamer. – Nolan Bushnell",
    "You miss 100 percent of the shots you don’t take – Wayne Gretzky",
    "Successful people are the ones who are breaking the rules. – Seth Godin",
    "To the degree we’re not living our dreams; our comfort zone has more control of us than we have over ourselves. – Peter McWilliams",
    "Saying no to loud people gives you the resources to say yes to important opportunities. – Seth Godin",
    "To think creatively, we must be able to look afresh at what we normally take for granted. – George Kneller",
    "Business in a combination of War and sport. – Andre Maurois",
    "To succeed… You need to find something to hold on to, something to motivate you, something to inspire you. – Tony Dorsett",
    "Always forgive your enemies. Nothing annoys them more. – Oscar Wilde",
    "All paid jobs absorb and degrade the mind. – Aristotle",
    "Winning is not a sometime thing; it’s an all time thing. You don’t win once in a while, you don’t do things right once in a while, you do them right all the time. Winning is habit. Unfortunately, so is losing. – Vince Lombardi",
    "There is no security on the earth, there is only opportunity. – General Douglas MacArthur",
    "Never put off until tomorrow what you can avoid altogether. – Unknown",
    "Hire character. Train skill. – Peter Schutz",
    "For maximum attention, nothing beats a good mistake. – Unknown",
    "Early to bed and early to rise probably indicates unskilled labor. – John Ciardi",
    "Don’t worry about people stealing your ideas. If your ideas are any good, you’ll have to ram them down people’s throats. – Howard Aiken",
    "It takes more than capital to swing business. You’ve got to have the A. I. D. degree to get by — Advertising, Initiative, and Dynamics. – Ren Mulford Jr.",
    "A calm sea does not make a skilled sailor. – Unknown",
    "The worst part of success is to try to find someone who is happy for you. – Bette Midler",
    "Your time is precious, so don’t waste it living someone else’s life. – Steve Jobs",
    "Only when the tide goes out do you discover who’s been swimming naked. – Warren Buffett",
    "Your income is directly related to your philosophy, NOT the economy. – Jim Rohn",
    "The NBA is never just a business. It’s always business. It’s always personal. All good businesses are personal. The best businesses are very personal. – Mark Cuban",
    "Fire the committee. No great website in history has been conceived of by more than three people. Not one. This is a deal breaker. – Seth Godin",
    "Once you free yourself from the need for perfect acceptance, it’s a lot easier to launch work that matters. – Seth Godin",
    "You have brains in your head. You have feet in your shoes. You can steer yourself, any direction you choose. – Dr. Seuss",
    "It is not the strongest of the species that survive, nor the most intelligent, but the one most responsive to change. – Charles Darwin",
    "The golden rule for every business man is this: Put yourself in your customer’s place. – Orison Swett Marden",
    "To win without risk is to triumph without glory. – Pierre Corneille",
    "People don’t believe what you tell them. They rarely believe what you show them. They often believe what their friends tell them. They always believe what they tell themselves. – Seth Godin",
    "A successful man is one who can lay a firm foundation with the bricks others have thrown at him. – David Brinkley",
    "Instead of wondering when your next vacation is, you ought to set up a life you don’t need to escape from. – Seth Godin",
    "I had to make my own living and my own opportunity! But I made it! Don’t sit down and wait for the opportunities to come. Get up and make them! – C.J. Walker",
    "People are best convinced by things they themselves discover. – Ben Franklin",
    "I don’t pay good wages because I have a lot of money; I have a lot of money because I pay good wages. – Robert Bosch",
    "Opportunity is missed by most people because it is dressed in overalls and looks like work. – Thomas Edison",
    "Everyone is a genius. But if you judge a fish by its ability to climb a tree, it will spend its whole life believing it is stupid – Einstein",
    "Do or do not. There is no try. – Yoda",
    "Your most unhappy customers are your greatest source of learning. – Bill Gates",
    "Leadership is doing what is right when no one is watching. – George Van Valkenburg",
    "In the business world, everyone is paid in two coins: cash and experience. Take the experience first; the cash will come later. – Harold Geneen",
    "Those who say it can not be done, should not interrupt those doing it. – Chinese Proverb",
    "Whatever the mind of man can conceive and believe, it can achieve. Thoughts are things! And powerful things at that, when mixed with definiteness of purpose, and burning desire, can be translated into riches. – Napoleon Hill",
    "To succeed in business, to reach the top, an individual must know all it is possible to know about that business. – J. Paul Getty",
    "A consultant is someone who takes the watch off your wrist and tells you the time. – Unknown",
    "The absolute fundamental aim is to make money out of satisfying customers. – John Egan",
    "If you would like to know the value of money, try to borrow some. – Benjamin Franklin",
    "If it really was a no–brainer to make it on your own in business there’d be millions of no–brained, harebrained, and otherwise dubiously brained individuals quitting their day jobs and hanging out their own shingles. Nobody would be left to round out the workforce and execute the business plan. – Bill Rancic",
    "Get busy living or get busy dying. – from the “The Shawshank Redemption”",
    "Winners take time to relish their work, knowing that scaling the mountain is what makes the view from the top so exhilarating. – Denis Waitley",
    "Statistics suggest that when customers complain, business owners and managers ought to get excited about it. The complaining customer represents a huge opportunity for more business. – Zig Ziglar",
    "The only place success comes before work is in the dictionary. – Vidal Sassoon",
    "Speak the truth, but leave immediately after. – Unknown",
    "The important thing is not being afraid to take a chance. Remember, the greatest failure is to not try. Once you find something you love to do, be the best at doing it. – Debbi Fields",
    "Are you a serial idea–starting person? The goal is to be an idea–shipping person. – Seth Godin",
    "The problem with the rat race is that even if you win, you’re still a rat. – Lilly Tomlin",
    "Success in business requires training and discipline and hard work. But if you’re not frightened by these things, the opportunities are just as great today as they ever were. – David Rockefeller",
    "If you work just for money, you’ll never make it, but if you love what you’re doing and you always put the customer first, success will be yours. – Ray Kroc",
    "Long–range planning works best in the short term. – Doug Evelyn",
    "Genius is one percent inspiration and ninety–nine percent perspiration. – Thomas A. Edison",
    "Intuitive design happens when current knowledge is the same as the target knowledge. - Jared Spool - Web Site Usability: A Designer's Guide"
]

export const getRandomQuote = () => {
    return quotes[Math.floor(Math.random() * quotes.length)];
}
export const quotesWithAuthor = quotes.map((quote) => { // split by ' - ' and map to object with quote and author
    const splitQuote = quote.split(' – ');
    return {
        quote: splitQuote[0] ?? '',
        author: splitQuote[1] ?? 'Unknown',
    }
})

export const getRandomQuoteWithAuthor = () => {
    return quotesWithAuthor[Math.floor(Math.random() * quotesWithAuthor.length)];
}

export const shuffle = <T>(array: T[]) => {
    let currentIndex = array.length,  randomIndex;
    while (currentIndex !== 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
    }
    return array;

}

export const highlightRandomText = (text: string, maxRatio = 3) => {
    const words = text.split(' ');
    const randomAmount = Math.min(words.length / maxRatio, Math.floor(Math.random() * words.length));
    const randomIndexes = new Set<number>();
    while (randomIndexes.size < randomAmount) {
        randomIndexes.add(Math.floor(Math.random() * words.length));
    }
    return words.map((word, index) => {
        if (randomIndexes.has(index)) {
            return `<span class="highlight">${word}</span>`;
        }
        return word;
    }).join(' ');
}
