import React from 'react'
import Layout from '../components/Layout'
import Typography from "@mui/material/Typography";
import {graphql, Link} from "gatsby";
export default ({data}: {data: unknown}) => {
    const nodes = data.allMarkdownRemark.nodes
    const posts = nodes.map((node: unknown) => ({
        id: node.parent.id,
        title: node.fields.title,
        fileDate: node.fields.fileDate,
        name: node.parent.name,
        excerpt: node.excerpt,
        link: node.frontmatter.layout === 'post' ? 'blog' : 'learnpp'
    }))
    return (
        <Layout>
            <Typography variant="h2">Recent</Typography>
            {posts.map((post: unknown) => (
                <article key={post.id}>
                    <Link to={'/' + post.link + '/' + encodeURIComponent(post.name)}>
                        <Typography variant="h4">{post.title}</Typography>
                        <Typography variant="subtitle1">Posted: {post.fileDate}</Typography>
                        <Typography variant="body1">{post.excerpt}</Typography>
                    </Link>
                </article>
            ))}
        </Layout>
    )
}

export const query = graphql`
query {
    allMarkdownRemark(
        sort: {fields: {fileDate: DESC}}
        limit: 5
    ) {
        nodes {
          parent {
            ... on File {
              id
              name
            }
          }
          fields {
            fileDate(formatString: "MMMM, DD YYYY")
            title
          }
          excerpt
          frontmatter {
            layout
          }
        }
    }
}
`
