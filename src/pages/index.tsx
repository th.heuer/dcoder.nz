import * as React from "react"
import {graphql, HeadFC, Link, PageProps} from "gatsby"
import Layout from '../components/Layout'
import Quotes from "../components/Quotes";
import {LatestBlog} from "../components/LatestBlog";
import Typography from "@mui/material/Typography";

const IndexPage: React.FC<PageProps> = (props: {data: unknown}) => {
  return <Layout>
    <Quotes />
    <LatestBlog data={props.data} />
    <Link to="/blog">
      <Typography variant="h3" sx={{
        display: 'block',
        color: 'text.primary',
        textDecoration: 'none',
        textDecorationLine: 'none',
        fontSize: '1.5rem',
        fontStyle: 'normal',
        paddingBottom: '4rem',
      }}>
        All Posts
        </Typography>
    </Link>
  </Layout>
}

export default IndexPage

export const Head: HeadFC = () => <title>Home Page</title>
export const query = graphql`
query {
    allMarkdownRemark(sort: {fields: {fileDate: DESC}}) {
        nodes {
          parent {
            ... on File {
              id
              name
            }
          }
          fields {
            fileDate(formatString: "MMMM, DD YYYY")
            title
          }
        }
    }
}
`
