import * as React from 'react'
import { graphql } from 'gatsby'
import Layout from '../../components/Layout'

const LeanPpPost = ({data}: {data: unknown}) => {
    return (
            <div>
                <Layout>
                    <article dangerouslySetInnerHTML={{__html: data.markdownRemark.html}} />
                </Layout>
            </div>
    )
}

// export const Head = () => <Seo title="Super Cool Blog Posts" />
export const query = graphql`
  query ($id: String) {
    markdownRemark(id: {eq: $id}) {
      frontmatter {
        title
      }
      html
    }
  }`
export default LeanPpPost
