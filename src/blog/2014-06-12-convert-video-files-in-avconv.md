---
layout: post
status: publish
published: true
title: Convert Video Files with avconv
author:
  display_name: dcodernz
  login: dcodernz
  email: no-reply@thh.com
  url: http://www.ihostnz.com
author_login: dcodernz
author_email: no-reply@thh.com
author_url: http://www.ihostnz.com
wordpress_id: 273
wordpress_url: http://www.thheuer.com/?p=273
date: '2014-06-12 15:30:42 +1200'
date_gmt: '2014-06-12 02:30:42 +1200'
categories:
- Howto
tags: [Howto,Video]
comments: []
---

It's always frustrating to find out how to convert video files from one format to another or resize your videos that you want to upload to youtube at maximum quality but minimal file size.

So, here some avconv settings to remember or use to convert an mkv file to avi (stereo):
```bash
avconv -i input.mkv -b:v 4000k -c:a \
libmp3lame -b:a 320k -ac 2 -s 720x480 out.avi


-i = input file
-b:v = bitrate video (4000k = 4MBit/s = 4000KBit/s)
-c:a = codec audio (libmp3lame = mp3)
-b:a = bitrate audio (320k = 320KBit/s)
-ac 2 = audio channels 2
-s 720x480 = size 480p
```
