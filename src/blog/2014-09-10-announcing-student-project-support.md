---
layout: post
status: publish
published: true
author:
  display_name: dcodernz
  login: dcodernz
  email: info@dcoder.co.nz
  url: https://dcoder.nz
author_login: dcodernz
author_email: info@dcoder.co.nz
author_url: https://dcoder.nz
categories:
- Announcement
tags: [Announcement,Student,Project,Support]
comments: []
date: "2014-09-10"
title: "Announcing student project support"
---

Are you a student looking for a company to do a class room project involving
Information Technology? If the answer is yes, read on!

[dcoder.nz](https://dcoder.nz) is now supporting
students through their studies with student
projects. You will need to provide a CV and cover letter to prove
your language proficiency and that you want to work
with us. This is necessary because we cannot take on everyone and need to
ensure we are not wasting our time. However, if you are in your last year
of study and think you've got what it takes to contribute to a successful
project, you are welcome and encouraged to apply. If you've made it this far,
you probably have the skills and perseverance to go on.

You can suggest your own project or we can probably provide you with an idea
that is doable within your 450 hour or similar time-frame. We have mentored
student projects before and are confident that we can guide you through a
successful project, that is interesting and doable.

Let it be an Android application, a web development project or a
new technology to develop, we can probably help you. So, talk to us.

Conditions are that the project is with a university or similar education
institution like Massey University or UCOL. The other condition is it is in
my home town, close or at least within the country.
If not within my home town, we require an informal meeting to take place
and some proof of identity and willingness to sign a contract which may be
provided by the educational institution.
