---
layout: post
status: publish
published: true
title: ! 'Howto: Postfix aliases setup email account'
author:
  display_name: dcodernz
  login: tim
  email: no-reply@thh.com
  url: ''
author_login: admin
author_email: no-reply@thh.com
wordpress_id: 59
wordpress_url: http://www.ihostnz.com/blog/?p=59
date: '2010-12-25 17:24:34 +1300'
date_gmt: '2010-12-25 05:24:34 +1300'
categories:
- Linux
- Howto
tags: [Howto,Linux,Email]
comments: []
---

<p>See here how to setup an email account with Postfix, the popular email server.</p>

<p>Edit /etc/postfix/virtual.cf .</p>
```bash
vim /etc/postfix/virtual.cf```

<p>Add line:</p>
```bash
user@example.com localusername```

```bash
postmap /etc/postfix/virtual.cf```

```bash
vim /etc/aliases```

<p>Add line:</p>
```bash
localusername: someother@othersite.com```

```bash
 ```

```bash
newaliases```

