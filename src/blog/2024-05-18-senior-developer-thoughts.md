---
layout: post
status: publish
published: true
title: "Senior Developer Thoughts"
author:
  display_name: dcodernz
  login: dcodernz
  email: info@dcoder.co.nz
  url: http://dcoder.nz
author_login: dcodernz
author_email: info@dcoder.co.nz
author_url: http://dcoder.nz
categories:
- Computer Science
- Senior Developer Thoughts
tags: [computer, science, senior, developer, thoughts]
comments: []
---
# Senior Developer Thoughts

This post is about my thoughts on what it means to be a senior developer.

This post was partially written by GitHub Copilot. Stay tuned for possibly a better version.
I will update the date in that case.

It was just too tempting to not use it. 😅

## Introduction

As a senior developer, I have been in the industry for a long time.
I have seen many changes and have learned a lot along the way.
In this post, I will share some of my thoughts on what it means to be a senior developer
and how you can become one.

## What is a Senior Developer?

A senior developer is someone who has a lot of experience in the industry.
They have worked on many projects and have seen a lot of different technologies.
They are able to solve complex problems and are able to work independently.
They are also able to mentor junior developers and help them grow in their careers.

## How to Become a Senior Developer

To become a senior developer, you need to have a lot of experience.
You need to work on many projects and learn as much as you can.
You need to be able to solve complex problems and be able to work independently.
You also need to be able to mentor junior developers and help them grow in their careers.

## Conclusion

Being a senior developer is not easy.
It takes a lot of hard work and dedication.
But if you are willing to put in the effort, you can become a senior developer.
And once you do, you will be able to help others grow in their careers as well.

## Written Mainly By Me

See how AI can be used but cannot replace the human touch. Admittedly, Copilot
is even helping me write this. But I found that Copilot cannot quite get to where you want
to go. In fact it can be distracting from what you really want to write, also with code.
So, you still need real developers to write and more so read code and other content.

## About this Blog

This is the first post since ages. I have been super busy. As a father of my child, who is now 2 years old,
and in an emancipated relationship, I have had my hands full for quite some time.
In the evenings I am tired and sleepy, and it is hard to find time and focus to write.

Also, this blog has been a project in the making since quite a while. I chose GatsbyJS as the
platform of choice since I have been working with React Native for work for some time.
Also, GatsbyJS is the most flexible for me. Hugo was another candidate, but I don't want to
learn Go. I rather went the Rust and C++ route on that level. 😅

I hope to write more in the future. I have a lot of ideas and thoughts to share and this blog is
my way of doing so.

Another thing that inspired me to recreate this blog is that I attended a Junior Developer
meetup and loved it. I felt really humbled and inspired by the people I connected with there.
Currently, a challenge was to create a personal project and I chose my blog.

## Perfection vs. Done

As a developer, you are never done. There is always room for improvement in code and other content you
write. You might notice that even this very post is far from perfect. It is basically just a stream
of thoughts, written down for you to digest.

However, there is the 80-20 rule, which means 80% of the result can usually be achieved 
with 20% of the effort. To be honest I now tend towards the 90-10 rule, which means 90% of the result
can usually be achieved with 10% of the effort. Of course this is not true. You cannot spend less effort
and achieve more. Or is it?

So, I put this blog together as quickly as I could, because it is more important to me to
share my thoughts than to create the perfect blog. If you have a look at the source code, you will
see that I have cut some corners. While that is not what I recommend for professional projects,
it can be a path for some personal projects.

This blog used to be a Jekyll blog, but I had not upgraded for a while and wanted to use something
that I know better and can customise more with. Sure, I had to learn GatsbyJS, but it was worth it
and probably easier than to learn Ruby on Rails and customise Jekyll or upgrade.

So, in the spirit of shipping this blog, I will end this post here. I hope you enjoyed it and
I hope to write more in the future. If you have any thoughts or feedback, feel free to reach out to me.

info@dcoder.co.nz

If you know me in person, please let me know how you heard about this blog. I am curious to know.
It is not really a secret, but I have not shared it with many people yet.

## The End

... for now. 😅
