---
layout: post
title: Hey Google - Build Me...
status: publish
published: true
author:
  display_name: dcodernz
  login: dcodernz
  email: no-reply@thh.com
  url: http://www.thheuer.com
author_login: dcodernz
author_email: no-reply@thh.com
author_url: http://www.thheuer.com
categories:
- Computer
- Science
- Ideas
- Factory
tags: [Computer, Science, Ideas, Factory, Hey, Google, Implement, This]
comments: []
---

"Hey Google! ... " is what many of us are saying now. This post is about a
meta idea for Google or companies like it. An idea factory factory if you
like.

### Hey Google! ... Build me ...

Every now and then we get ideas. We think: "This is going to make me rich!".
We think: "I should protect this idea with a patent or similar.".

However, many people have many ideas and when someone tells you their idea
it might sound very convincing in an evening over a few beers or wine. It
might sound like if you do it and keep it secret, you are indeed going to
get rich (quick!). This is a fallacy. Unless you already have a substantial
amount of capital raised and have full-time or more and the ability to get
people to do things for you either with money or other motivations.

Having said that, if it weren't for people who have the drive and a vision
to fulfill their or many people's dreams, it would be a pretty boring place
and progress would not happen