---
layout: post
status: publish
published: true
title: "Search Engine Optimization<br/>Google - Duckduckgo - Bing"
author:
  display_name: dcodernz
  login: dcodernz
  email: info@dcoder.co.nz
  url: http://dcoder.nz
author_login: dcodernz
author_email: info@dcoder.co.nz
author_url: http://dcoder.nz
categories:
- Web-Development
tags: [google,search,engine,optimization,seo,duckduckgo,bing,tips,tricks]
comments: []
---

If you are looking for how to increase your websites online visibility and get it up on the organic results, you found some valuable tips.

Website Search Engine Optimization
==================================

This post aims to get up high on [Google](https://www.google.com) by using its own tips. Just study the page itself and if you found this page on [Google](https://www.google.com), [Duckduckgo](https://duckduckgo.com/), [Microsoft Bing](https://www.bing.com/) or another major [search engine](https://en.wikipedia.org/wiki/Web_search_engine), chances are that the tips here really work.

In a way this is a [meta search engine](https://www.google.com) meta post.

Get up on Google, Bing, Duckduckgo and other Search Engines
===========================================================

If you want to get your [website](https://en.wikipedia.org/wiki/Website) up on [Google](https://www.google.com) or other [search engines](https://en.wikipedia.org/wiki/Web_search_engine) like [Microsoft Bing](https://www.bing.com/) or even [Duckduckgo](https://duckduckgo.com/), you may have a long road ahead of you. Use these definite tips to ease the ride.

Now that many people have figured out the [organic search](https://en.wikipedia.org/wiki/Organic_search) is one of the most important things for your web site, it is not a simple [copy/paste](https://en.wikipedia.org/wiki/Cut,_copy,_and_paste) job any more. You need to invest quite some time, effort and thought into the process.

Howto: 15 Tips On Getting Up On Search Engines
----------------------------------------------

1. Follow guides that are already first in the very [search results](https://www.google.com/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=search%20engine%20optimization).
2. Make sure you get a high [Google back link count](#google-back-link-count).
3. Ensure good spelling and grammar use, [spell check your web site](http://www.internetmarketingninjas.com/online-spell-checker.php).
4. Choose the right [key words]().
5. Be to the point and don't waste your readers' time with irrelevant information.
6. Dedicate each page carefully to its most important theme.
7. Don't waste your time with too much meta information in the &lt;head/&gt; section.
8. Post on forums and other websites about the topic that is on your website ensuring a link is posted back to your site.
9. Keep posting new and relevant content on your website.
10. Have a fast loading website.
11. Enable [HTTPS/SSL](https://en.wikipedia.org/wiki/HTTPS) to make your site secure and make it impossible for routers to inject unwanted content such as 3rd party advertisement.
12. Link to [other relevant websites](http://www.forbes.com/sites/deborahljacobs/2013/07/01/10-ways-to-improve-your-google-rank/#2980a79416cb) on the topic (search engine optimization).
13. Have a mobile friendly site which follows [mobile usability guidelines](https://support.google.com/webmasters/answer/6101188?hl=en).
14. Make links have a relevant content to what they actually are like you see in this page.
15. Hire someone to do the job: I happen to offer [professional website development services](/2014/07/announcing-web-development/).

More Information on SEO - Search Engine Optimization
----------------------------------------------------

If you want more detail on the above, read this section:

This is a self-improving article which will need some work over the coming months if not years and will need an evolution-like development. So, watch this space. This website has an [RSS Feed](/feed.xml), so please [subscribe](/feed.xml). A nice reader is the [Feedly RSS Reader](https://feedly.com/i/welcome).


Relevant Links
--------------

[Robots.txt Checker](https://www.websiteplanet.com/webtools/robots-txt/)