---
layout: post
status: publish
published: true
author:
  display_name: dcodernz
  login: dcodernz
  email: no-reply@thh.com
  url: http://www.thheuer.com
author_login: dcodernz
author_email: no-reply@thh.com
author_url: http://www.thheuer.com
categories:
- Howto
tags: [Howto,Jekyll,CMS,Installation,Tagging,Blogging,Ubuntu]
comments: []
date: "2014-07-06"
title: "Jekyll installation"
---

Jekyll is the software I'm using for this blog. So, just as a reminder for myself and people who want to try it out, I thought I would document here, how to install it.

You can simply follow these steps:

```bash

sudo apt-get install -y ruby1.9.1-dev && \
sudo gem install jekyll && \
sudo gem install jekyll-tagging
```


That's all! Now you can create your sites with the easy to use content management system.
