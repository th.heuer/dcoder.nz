---
layout: post
status: publish
published: true
title: "Computer Science - What it really is?!"
author:
  display_name: dcodernz
  login: dcodernz
  email: info@dcoder.co.nz
  url: http://dcoder.nz
author_login: dcodernz
author_email: info@dcoder.co.nz
author_url: http://dcoder.nz
categories:
- Philosophy
- Computer Science
- Social
tags: [computer,science,philosophy]
comments: []
---

Computer Science - The Art of Moving Bits
What do we do all day as programmers? Is it worth the effort? Does it mean anything?

Computer Science - What it really is?!
=====================================

Computer Science is the art of computing. But what is it really, underneath all the
fancy theory and how to do things so we can achieve some kind of information transformation?

Computer science is fun. It challenges the underlying theories of how things work. The universe, maybe?
Logic and mathematics are fundamental building blocks on how to transform data from one format to the other.
Data is just a number or a series of numbers, which essentially form another larger number.
What does it really mean though? We basically just push bits around this glorified machine
called the computer.
We basically just copy data from A to B and sometimes do an operation on it. Basically,
we are just performing additions or even more primitavely, we perform logical operations
that can be boiled down to 2 operations (see deMorgan).

But we think it can change the world. But can it? I like to think it can. The universe is ruled
by the rules of logic, at least as far as we know. If science is right, and it sure looks like it
is, the universe is made up of some kind of unit, lets call it atoms, electrons and/or protons.

The physical laws follow logical patterns and results gained from calculators
and ultimately computers can model our world in a way that not even our brain can
keep up with. Models, even though they are pure simplifications of the real world,
could predict global warming, make our traffic more or less work and help us 
optimise our lives and make them simpler in any way imaginable. It is undeniable
that information technology has transformed our lives in ways we couldn't even
imagine just a few years back. We now have satellites, orbiting the earth, the Internet, giving us
access to a vast amount of information about our world and a lot of other 
technology that (supposedly) eases our lives. We are too intelligent for our own
good really. We know what is happening in the world and sometimes get depressed
thinking about all those people living a much worse life than ours as well as
people who live a better one.

What is it good for? Well, I like to think that computer science can help us
lead a happier life, appreciating what we have. We have a functioning mind for 
starters. That is already a great privilege. Computer Science is not about how
to make the next million $$ idea work, it is about comprehending the world just
that little bit more, how things work, why things work. For me, computer science
has given me purpouse. In a world where everything seems like it has been discovered,
computer science gives one the notion that maybe, just maybe, we don't know everything
yet. In fact, I don't think everything is discoverable. Mathematicians have
stated that not even most problems can be solved. Most problems, in theory,
have no solution, at least not computable in a feasible amount of time.

I could dabble on for more, but I just want to make you think of the bare bones
of our existence.

To me, life is about achieving real happiness. To me, happiness comes from having
purpouse in your life, which seems so hard to achieve with minds that have existed
already, that are much stronger, smarter or better than mine. However, I can make
a positive difference in this world by just treating people in a good way and
doing something that I enjoy. I enjoy working with computers, because I feel
there is enough bullshit in the world and logic, mathematics and deductively
computer science minimise the amount of bs.

So, I will close with: I love learning new things about computers and solving
real problems with them, because it makes me feel special. Everyone should
feel special. So, consider going on youtube or any other website and learning
about this very creative, logical and beautiful subject called

COMPUTER SCIENCE.

All the best,
dcodernz
