---
title: Digital Education - Learning Programming
layout: post
status: publish
published: true
author:
  display_name: dcodernz
  login: dcodernz
  email: info@dcoder.co.nz
  url: http://www.dcoder.co.nz
author_login: dcodernz
author_email: info@dcoder.co.nz
author_url: http://www.dcoder.co.nz
categories:
- Digital Education
tags: [Programming,Language,Digital,Education]
comments: []
---

Learning how to program is something that is very scary for most people. It is
still a rare skill and people get good, fun and well paid jobs in this industry.
Therefore dcoder is launching a digital education programme.

Programming is a skill that will be saught after for years to come. Learn to
learn. Learn to program. Learn to like learning and consult dcoder for
one-on-one programming lessons. Dcoder will adapt to your skill level, your
learning pace and make most efficient use of your time.

This is an offering of a course to learn programming in your language of choice
given that I know the language. My main programming languages are Java,
JavaScript, C(++), Python, PHP and now Dart. I believe strongly, that you can
easily learn a new language if you know one. So, make a choice and learn your
first programming language.

Programming is fun. You can do a lot of things with this skill. You can impress
your friends and find well-paid and fun jobs with this skill.

So, without further ado. I am offering lessons for an astonishing reasonable
price:

**NZ$50 for a one-on-one 1 hour lesson**

If you find peers to join you, I will teach

**2 people for NZ$40 per hour each**

**3 people for NZ$30 per hour each**

**4 people for NZ$25 per hour each**

I look forward to teaching or helping you learn this future-proof and fun skill.

All the best,
dcodernz
