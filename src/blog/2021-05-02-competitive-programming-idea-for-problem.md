---
layout: post
status: publish
published: true
title: "Competitive Programming - Idea for Interactive Problem"
author:
  display_name: dcodernz
  login: dcodernz
  email: info@dcoder.co.nz
  url: http://dcoder.nz
author_login: dcodernz
author_email: info@dcoder.co.nz
author_url: http://dcoder.nz
categories:
- CP
- Computer Science
tags: [computer,science,cp]
comments: []
---

Interactive Competitive Programming problem.

Competitive Programming - Idea for Interactive Problem
======================================================

Interactive problems are becoming more interesting as many sites now support them.

I just had an idea for an interactive problem:

I won't say what the solution is, as it would ruin it for people wanting to solve
it.

Now, let's get to it!

You are a scammer and are trying to get as much money as possible from your victims.

You pretended to transfer S dollars into their account.

However, you have a problem: You made your victim's bank account appear as if you
had transferred X amount of dollars into their bank account. However, of course the
money did not get there because you just faked it.

So, you have to try and get as much money as possible from your victim and minimising
the amount of transaction attempts that you do to tranfer Y dollars "back" to your account. You need to minimise the amount of trial transfers and maximise the amount
of money that you try to get out of them. If a transfer is successful, the function
will return true. If it is unsuccessful, you will receive false.

Remember if successful, the money will be deducted from their balance