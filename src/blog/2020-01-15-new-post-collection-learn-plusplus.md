---
layout: post
title: New Post Collection! - Learn++
status: publish
published: true
author:
  display_name: dcodernz
  login: dcodernz
  email: no-reply@thh.com
  url: http://www.thheuer.com
author_login: dcodernz
author_email: no-reply@thh.com
author_url: http://www.thheuer.com
categories:
- Learn++
tags: [Learn++]
comments: []
---
Want to [learn something new every day](/learn++)? My challenge: Document what I learned
in a short post every day.

### Or as often as possible

Every day we learn something new they say. I try to do that and so that I don't
forget and share my knowledge. Here are my micro posts:

[learn++](/learn++)