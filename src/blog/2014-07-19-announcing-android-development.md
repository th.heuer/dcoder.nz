---
layout: post
status: publish
published: true
author:
  display_name: dcodernz
  login: dcodernz
  email: info@dcoder.co.nz
  url: http://www.dcoder.co.nz
author_login: dcodernz
author_email: info@dcoder.co.nz
author_url: http://www.dcoder.co.nz
categories:
- Announcement
tags: [Announcement,Android,Development,Mobile]
comments: []
date: "2014-07-19"
title: "Announcing android development"
---

We're proudly announcing Android Development to be a focus from now into the
future. Android is growing fast and we're committed to embracing this fast
growth by specialising on this platform.

There are also other platforms for mobile, such as HTML5 Cordova and of course
iOS (iPhone, iPad, ...). However, Android is an open platform. The source code
is freely available and is Open Sourced. Also, Android is the fastest growing
mobile platform and at its core is the Linux OS. Another strong reason for this
decision is that Android Natively supports Java and we've been specialising in
Java Development for years. Additionally, Android support XML and a styling
language similar to CSS which is why our web development background helps a lot.
Learning Android proves to be fast an easy, so we can easily be productive
quickly.

Let us help you grow in this exciting area by deciding to develop your
app with us. We look forward to your feedback and working with you.

Kind regards,
The Android Team
