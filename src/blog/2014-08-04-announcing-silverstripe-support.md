---
layout: post
status: publish
published: true
author:
  display_name: dcodernz
  login: dcodernz
  email: info@dcoder.co.nz
  url: http://www.dcoder.co.nz
author_login: dcodernz
author_email: info@dcoder.co.nz
author_url: http://www.dcoder.co.nz
categories:
- Announcement
tags: []
comments: []
date: "2014-08-04"
title: "Announcing silverstripe support"
---

We are dedicated to support SilverStripe, the relatively new and very well 
designed Content Management System. We can develop your sites, your web apps
and mobile sites with this great piece of technology. It supports easy
extensibility and has great template support. Furthermore,
it is a New Zealand made and Free and Open Source Software.

It is written in PHP and has a lot of templates and modules out of the box 
which are very easy to extend and manipulate. The framework is well-designed 
and it has a large community of developers and users.

Launch your website or application with us and you can take advantage of 
competitive, rapid web development with a website or application that you can
update yourself. You will only need us to develop new features and extensions
and occasionally for support.

Watch [@geekdenz](http://www.silverstripe.org/ForumMemberProfile/show/36449) 
on [SilverStripe.org](http://www.silverstripe.org) for proof of work and 
competency. You can ask questions directly there and if you need more support
you can get it directly at dcoder.nz by [getting in touch](/contact/).