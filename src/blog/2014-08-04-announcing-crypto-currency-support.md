---
layout: post
status: publish
published: true
author:
  display_name: dcodernz
  login: dcodernz
  email: info@dcoder.co.nz
  url: http://www.dcoder.co.nz
author_login: dcodernz
author_email: info@dcoder.co.nz
author_url: http://www.dcoder.co.nz
categories:
- Announcement
tags: [Crypto,Currency,Bitcoin]
comments: []
date: "2014-08-04"
title: "Announcing crypto currency support"
---

Crypto Currency is an uprising technology for payment. It is still in its 
infancy and by some regarded as a ponzi scheme. However, I read the 
[paper on Bitcoin](https://bitcoin.org/bitcoin.pdf) and it makes a lot of sense.
In fact, I believe it could potentially be better than the "real" money we 
currently use because of its mathematical properties.

So, even though it is not "real" money, I've decided to accept it as a payment
option in exchange for services, at least for a while. As a promotion, I will
accept a 20% discount if using Bitcoin.

Other Crypto Currency could also be accepted, but I would need to check its
validity first.

You can view the real value of, for example, Bitcoin 
[here](https://www.google.co.nz/search?q=bitcoin+value&oq=bitcoin+value&aqs=chrome..69i57j0j69i60l2j69i59j0.4785j0j4&sourceid=chrome&es_sm=93&ie=UTF-8).

So, that means one can exchange it for real money. However, transaction costs
are a lot lower and it is much more convenient over great distances.

20% off means 80% of the normal price at an agreed time. This could be time
of invoice or potentially earlier to provide a fair game.
