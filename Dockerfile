FROM ubuntu:22.04
# FROM ubuntu:20.04

RUN apt update
RUN apt install -y python3 ruby-full build-essential zlib1g-dev
#COPY Gemfile* /
#RUN gem install bundler
# RUN bundle install

RUN echo '# Install Ruby Gems to ~/gems' >> /root/.bashrc
RUN echo 'export GEM_HOME="$HOME/gems"' >> /root/.bashrc
RUN echo 'export PATH="$HOME/gems/bin:$PATH"' >> /root/.bashrc
CMD bash -c 'while true; do sleep 60; done'
