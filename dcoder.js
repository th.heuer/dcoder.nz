(function($) {
	$('ul.posts h2 a,ul.posts p a,article.post-content a').each(function(i, e) {
		var l = document.location,
				origin = l.origin;
		if (e.href.substring(0, origin.length) !== origin) {
			$(e).attr('target', '_blank');
		}
	});
	function shuffle(array) {
		var currentIndex = array.length
				, temporaryValue
				, randomIndex
				;

		// While there remain elements to shuffle...
		while (0 !== currentIndex) {

			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	}
	var quotes = [];
	$('.quotes p').each(function(i, e) {
		quotes.push($(e).html());
	});
	shuffle(quotes);
	var index = 0,
			len = quotes.length;
	function mod(i, max) {
		while (i < 0) {
			i += max;
		}
		return i % max;
	}
	function showQuote() {
		$('.bg blockquote').fadeOut(function() {
			$(this).html('<p>' + quotes[index] + '</p>').fadeIn();
		});
	}
	$('.bg blockquote').html('<p>' + quotes[index] + '</p>');
	var interval;
	function makeInterval() {
		interval = setInterval(function() {
			index = ++index % len;
			showQuote();
		}, 9000);
	}
	makeInterval();
	$('.back').click(function() {
		index = mod(index - 1, len);
		showQuote();
		clearInterval(interval);
		makeInterval();
	});
	$('.forward').click(function() {
		index = mod(index + 1, len);
		showQuote();
		clearInterval(interval);
		makeInterval();
	});



})(jQuery);