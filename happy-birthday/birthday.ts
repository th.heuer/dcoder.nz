/// <reference path="typings/tsd.d.ts" />

const RECOLOR_DELAY = 300;
const FIREWORKS_RANGE = 2000;


declare function createFirework(blastRadius: number, particles: number, circles: number, burtType: number, startX: number, startY: number, explodeX: number, explodeY: number, one: boolean, two: boolean): void;

let highest: number[] = [];

function defaults(...args: number[]) {
	for (let num of args) {
		highest.push(num);
	}
}
function random(args: number[]): any[] {
	let newArgs = args.map((arg: number) => {
		return Math.ceil(Math.random() * arg);
	});
	return newArgs;
}

defaults(100, 200, 8, 7, 100, 100, 100, 100);


function makeFirework() {
	setTimeout(() => {
		let numbers = random(highest);
		let booleans = [];
		booleans.push(Math.random() < 0.5 ? true : false);
		booleans.push(Math.random() <= 0.5 ? true : false);
		createFirework.apply(numbers.concat(booleans));
		makeFirework();
	}, Math.ceil(Math.random() * FIREWORKS_RANGE));
}
makeFirework();

let search = decodeURIComponent(document.location.search.substring(1)).replace(/[+]/g, ' ');
let commaIndex = search.indexOf(',');
let parts = search.split(',');
let yourName = parts[0];
let line2 = parts.length > 1 ? parts[1] : '';
function randomColorValue(): number {
	let colorValue = Math.ceil(Math.random() * 255);
	return Math.max(colorValue, 100);
}
function letter(l: string): string {
	let r = randomColorValue();
	let g = randomColorValue();
	let b = randomColorValue();
	return '<span style="color: rgb('+r+','+g+','+b+')">'+ l +'</span>';
}
function getMarked(text: string): string {
	let markedUp = '';
	for (let i = -1, len = text.length; ++i < len;) {
		let c = text.substring(i, i + 1);
		if (c == ' ') {
			markedUp += c;
			continue;
		}
		markedUp += letter(c);
	}
	return markedUp;
}

function recolor() {
	let text = 'HAPPY BIRTHDAY ' + yourName +'!';
	$('#name').html(getMarked(text));
	$('#line2').html(getMarked(line2));
	setTimeout(recolor, RECOLOR_DELAY);
}
let a = 1;
let b = 1;
let $el = $('#content-happy');
let centerx = $(window).innerWidth() / 2 - $el.width() / 2;
let centery = $(window).innerHeight() / 2;
let i = 0;
let j = 1;
function move() {
	let angle = 0.1 * i;
	let x = centerx + (a + b * angle) * Math.cos(angle);
	let y = centery + (a + b * angle) * Math.sin(angle);
	$el.css('left', x + 'px');
	$el.css('top', y + 'px');
	if (i == 1200) {
		j = -1;
	} else if (i == 0) {
		j = 1;
	}
	i += j;
}
$('title').html('HAPPY BIRTHDAY '+ yourName +'!')
recolor();
setInterval(move, 1000/60);