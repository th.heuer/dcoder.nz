/// <reference path="typings/tsd.d.ts" />
var RECOLOR_DELAY = 300;
var FIREWORKS_RANGE = 2000;
var highest = [];
function defaults() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i - 0] = arguments[_i];
    }
    for (var _a = 0; _a < args.length; _a++) {
        var num = args[_a];
        highest.push(num);
    }
}
function random(args) {
    var newArgs = args.map(function (arg) {
        return Math.ceil(Math.random() * arg);
    });
    return newArgs;
}
defaults(100, 200, 8, 7, 100, 100, 100, 100);
function makeFirework() {
    setTimeout(function () {
        var numbers = random(highest);
        var booleans = [];
        booleans.push(Math.random() < 0.5 ? true : false);
        booleans.push(Math.random() <= 0.5 ? true : false);
        createFirework.apply(numbers.concat(booleans));
        makeFirework();
    }, Math.ceil(Math.random() * FIREWORKS_RANGE));
}
makeFirework();
var search = decodeURIComponent(document.location.search.substring(1)).replace(/[+]/g, ' ');
var commaIndex = search.indexOf(',');
var parts = search.split(',');
var yourName = parts[0];
var line2 = parts.length > 1 ? parts[1] : '';
function randomColorValue() {
    var colorValue = Math.ceil(Math.random() * 255);
    return Math.max(colorValue, 100);
}
function letter(l) {
    var r = randomColorValue();
    var g = randomColorValue();
    var b = randomColorValue();
    return '<span style="color: rgb(' + r + ',' + g + ',' + b + ')">' + l + '</span>';
}
function getMarked(text) {
    var markedUp = '';
    for (var i_1 = -1, len = text.length; ++i_1 < len;) {
        var c = text.substring(i_1, i_1 + 1);
        if (c == ' ') {
            markedUp += c;
            continue;
        }
        markedUp += letter(c);
    }
    return markedUp;
}
function recolor() {
    var text = 'HAPPY BIRTHDAY ' + yourName + '!';
    $('#name').html(getMarked(text));
    $('#line2').html(getMarked(line2));
    setTimeout(recolor, RECOLOR_DELAY);
}
var a = 1;
var b = 1;
var $el = $('#content-happy');
var centerx = $(window).innerWidth() / 2 - $el.width() / 2;
var centery = $(window).innerHeight() / 2;
var i = 0;
var j = 1;
function move() {
    var angle = 0.1 * i;
    var x = centerx + (a + b * angle) * Math.cos(angle);
    var y = centery + (a + b * angle) * Math.sin(angle);
    $el.css('left', x + 'px');
    $el.css('top', y + 'px');
    if (i == 1200) {
        j = -1;
    }
    else if (i == 0) {
        j = 1;
    }
    i += j;
}
$('title').html('HAPPY BIRTHDAY ' + yourName + '!');
recolor();
setInterval(move, 1000 / 60);
//# sourceMappingURL=birthday.js.map