const fs = require('fs');
const path = require('path');
const glob = require('glob'); // npm install glob

// This function converts Jekyll highlight blocks to markdown code blocks
function convertHighlightsToCodeBlocks(content) {
  return content.replace(/{% highlight (\w+) %}/g, '```$1\n').replace(/{% endhighlight %}/g, '```\n');
}

// Function to extract and format the front matter for Gatsby's MDX format
function formatFrontMatter(frontMatter, fileName) {
  const date = fileName.match(/\d{4}-\d{2}-\d{2}/)[0];
  const title = fileName.split(date + '-')[1].replace(/-/g, ' ').replace(/\.md$/, '');
  const yamlFrontMatter = frontMatter
    .replace(/^---/, '')
    .replace(/---$/, '')
    .trim();

  if (yamlFrontMatter.includes('title:')) {
    return `---
${yamlFrontMatter}
---`;
  }
  return `---
${yamlFrontMatter}
date: "${date}"
title: "${title.charAt(0).toUpperCase() + title.slice(1)}"
---`;
}

// Convert Jekyll Markdown to Gatsby MDX for all files in a directory
function convertJekyllToGatsby(sourceDir, outputDir) {
  // Using glob to read all markdown files in the source directory
  glob(`${sourceDir}/*.*`, (err, files) => {
    if (err) {
      console.error('Error reading files:', err);
      return;
    }

    files.forEach((file) => {
      const content = fs.readFileSync(file, 'utf8');
      const fileName = path.basename(file);

      // Split the content into front matter and body
      const parts = content.split(/^---\s*$/m);
      const frontMatter = parts[1];
      let body = parts[2] || '';

      // Transform highlight blocks to code blocks
      body = convertHighlightsToCodeBlocks(body);
      const formattedFrontMatter = formatFrontMatter(frontMatter, fileName);

      // Combine the new front matter and body
      const newContent = `${formattedFrontMatter}\n${body}`;

      // Write the new content into the output directory with a .mdx extension
      const newFilePath = path.join(outputDir, fileName.replace(/\..+$/, '.md'));
      fs.writeFileSync(newFilePath, newContent, 'utf8');
      console.log(`Converted ${file} to ${newFilePath}`);
    });
  });
}

// Specify the source directory containing the original Jekyll markdown files
// and the output directory where the transformed Gatsby MDX files will be saved
const sourceDirectory = process.argv[2] || '_posts'; // Update with the actual source directory path
const outputDirectory = process.argv[3] || 'src/blog'; // Update with the actual output directory path

// console.log(sourceDirectory, outputDirectory)
// Call the function to start the conversion process
convertJekyllToGatsby(sourceDirectory, outputDirectory);
