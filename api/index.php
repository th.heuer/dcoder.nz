<?php

define('API_KEY', 'da39a3ee5e6b4b0d3255bfef95601890afd80709');

$data = json_decode(file_get_contents('php://input'), true);

if (!isset($data['apiKey']) || $data['apiKey'] !== API_KEY) {
	die('Access Denied');
}

$method = isset($data['method']) ? $data['method'] : false;

function send_email($data) {
	if (!isset($data['from']) || !isset($data['to']) || !isset($data['subject']) || !isset($data['message']) || !isset($data['from'])) {
		die('"from" and "to" required');
	}
	mail($data['to'], $data['subject'], $data['message'], "From: ". $data['from']);
}
switch ($method) {
	case 'mail':
		send_email($data);
		break;
	default:
		die('"method" required');
}

