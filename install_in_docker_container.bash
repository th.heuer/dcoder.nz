#!/bin/bash

cd /app
gem install bundler
bundle install
jekyll serve -w --host 0.0.0.0