import type { GatsbyConfig } from "gatsby";
import path from 'path'

const gatsbyRequiredRules = path.join(
    process.cwd(),
    "node_modules",
    "gatsby",
    "dist",
    "utils",
    "eslint-rules"
);

const config: GatsbyConfig = {
  siteMetadata: {
    title: `dcoder.nz`,
    siteUrl: `https://dcoder.nz`,
  },
  // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
  // If you use VSCode you can also use the GraphQL plugin
  // Learn more at: https://gatsby.dev/graphql-typegen
  graphqlTypegen: true,
  plugins: ["gatsby-plugin-sass", "gatsby-plugin-image", "gatsby-plugin-sitemap", {
    resolve: 'gatsby-plugin-manifest',
    options: {
      "icon": "src/images/icon.png"
    }
  }, "gatsby-plugin-mdx", "gatsby-transformer-remark", "gatsby-plugin-sharp", "gatsby-transformer-sharp", {
    resolve: 'gatsby-source-filesystem',
    options: {
      "name": "images",
      "path": "./src/images/"
    },
    __key: "images"
  },{
    resolve: 'gatsby-source-filesystem',
    options: {
      name: 'blog',
      path: './src/blog'
    },
    __key: 'blog',
  },{
    resolve: 'gatsby-source-filesystem',
    options: {
      name: 'learnpp',
      path: './src/learnpp'
    },
    __key: 'learnpp',
  },{
    resolve: `gatsby-plugin-google-gtag`,
    options: {
      // You can add multiple tracking ids and a pageview event will be fired for all of them.
      trackingIds: [
        "GA-TRACKING_ID", // Google Analytics / GA
        "AW-CONVERSION_ID", // Google Ads / Adwords / AW
        "DC-FLOODIGHT_ID", // Marketing Platform advertising products (Display & Video 360, Search Ads 360, and Campaign Manager)
      ],
      // This object gets passed directly to the gtag config command
      // This config will be shared across all trackingIds
      gtagConfig: {
        optimize_id: "OPT_CONTAINER_ID",
        anonymize_ip: true,
        cookie_expires: 0,
      },
      // This object is used for configuration specific to this plugin
      pluginConfig: {
        // Puts tracking script in the head instead of the body
        head: false,
        // Setting this parameter is also optional
        respectDNT: true,
        // Avoids sending pageview hits from custom paths
        exclude: ["/preview/**", "/do-not-track/me/too/"],
        // Defaults to https://www.googletagmanager.com
        origin: "YOUR_SELF_HOSTED_ORIGIN",
        // Delays processing pageview events on route update (in milliseconds)
        delayOnRouteUpdate: 0,
      },
    },
  },
  //   {
  //   resolve: "gatsby-plugin-eslint",
  //   options: {
  //     // Gatsby required rules directory
  //     rulePaths: [gatsbyRequiredRules],
  //     // Default settings that may be omitted or customized
  //     stages: ["develop"],
  //     extensions: ["js", "jsx", "ts", "tsx"],
  //     exclude: ["node_modules", "bower_components", ".cache", "public"],
  //   },
  // },
    'gatsby-plugin-dark-mode',
    {
      resolve: `gatsby-plugin-fusejs`,
      options: {
        query: `
          {
            allMarkdownRemark {
              nodes {
                id
                rawMarkdownBody
                excerpt
                frontmatter {
                  title
                }
              }
            }
          }
        `,
        keys: ['title', 'body'],
        normalizer: ({ data }) =>
            data.allMarkdownRemark.nodes.map((node) => ({
              id: node.id,
              title: node.frontmatter.title,
              body: node.rawMarkdownBody,
              excerpt: node.excerpt,
            })),
      },
    },
  ]
};

export default config;
