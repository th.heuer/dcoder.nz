#!/bin/bash
JEKYLL_VERSION=3.4
#docker run --rm \
#  -p 4000:4000 \
#  --mount "source=$PWD,target=/srv/jekyll,type=bind" \
#  -it jekyll/jekyll:$JEKYLL_VERSION \
#  /bin/bash -c '/sbin/apk add python && /bin/bash'
#  #/bin/bash -c 'apk add python && /bin/bash'
docker run --rm \
  -p 4000:4000 \
  --mount "source=$PWD,target=/srv/jekyll,type=bind" \
  -it dcoder/jekyll \
  /bin/bash
